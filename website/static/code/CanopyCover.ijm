// ImageJ macro to put together binary histogram counts
// This assumes that the canopy photos have been converted
// to binary by hand (sometimes sky and cover is not
// separated automatically, e.g. cloud cover) and that black
// represents cover is added to file name (might need to
// edit > invert colors manually in ImageJ).


// Add a directory to suppress the getDirectory dialog
projectdir = "/Users/matt_l/Projects/periphyton-project/" // e.g. "C:\\Temp\\" or "C:/Temp/"

datadir = projectdir + "data/"
picdir = datadir + "canopy.pictures/"


// change .txt to .csv if you want to load it with Excel by a simple double click
path = datadir + "cover.txt";

// Create a header
File.delete(path)
File.append("label\topen\tclose", path);


// Batch mode
setBatchMode(true);

list = getFileList(picdir);
for (i = 0; i < list.length; i++){

    // Get the counts for 0 (open) and 255 (closed)

    open(picdir + list[i]);
    label = getTitle();

    getHistogram(values, counts, 256);
    n = 0;

    for (j = 0; j < 256; j++) {
    	count = counts[j];
	      if (count > 0) {
       	      	 n += count;
    	    	 }
	}

    openC=counts[0];
    closedC=counts[255];

    // This is the line that writes label, a,b and c to results.txt
    File.append(label + "\t" + openC + "\t" + closedC, path);

}

setBatchMode(false);

// Quit Fiji (imageJ)
run("Quit");